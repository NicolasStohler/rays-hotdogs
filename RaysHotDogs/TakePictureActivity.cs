using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using Android.Provider;
using RaysHotDogs.Utility;
using Android.Graphics;

namespace RaysHotDogs
{
	[Activity(Label = "Take a picture with Ray")]
	public class TakePictureActivity : Activity
	{
		private ImageView rayPictureImageView;
		private Button takePictureButton;
		private static File imageDirectory;
		private static File imageFile;
		private Bitmap imageBitmap;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.TakePictureView);

			FindViews();

			HandleEvents();

			imageDirectory = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(
				Android.OS.Environment.DirectoryPictures), "RaysHotDogs");

			if (!imageDirectory.Exists())
			{
				imageDirectory.Mkdirs();
			}

			//if (imageBitmap != null)
			//{
			//	rayPictureImageView.SetImageBitmap(imageBitmap);
			//}

			if(savedInstanceState != null)
			{
				imageBitmap = (Bitmap)savedInstanceState.GetParcelable("bitmap");
				rayPictureImageView.SetImageBitmap(imageBitmap);
			}

		}

		private void FindViews()
		{
			rayPictureImageView = FindViewById<ImageView>(Resource.Id.rayPictureImageView);
			takePictureButton = FindViewById<Button>(Resource.Id.takePictureButton);
		}

		private void HandleEvents()
		{
			takePictureButton.Click += TakePictureButton_Click;
		}

		private void TakePictureButton_Click(object sender, EventArgs e)
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			imageFile = new File(imageDirectory, String.Format("PhotoWithRay_{0}.jpg", Guid.NewGuid()));
			intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(imageFile));
			StartActivityForResult(intent, 0);
		}

		protected override void OnDestroy()
		{
			// imageBitmap = null;
			GC.Collect();
			base.OnDestroy();
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			//int height = rayPictureImageView.Height;
			//int width  = rayPictureImageView.Width;

			int height = Math.Max(Resources.DisplayMetrics.HeightPixels, 100);
			int width  = Math.Max(rayPictureImageView.Height, 100);

			imageBitmap = ImageHelper.GetImageBitmapFromFilePath(imageFile.Path, width, height);

			if (imageBitmap != null)
			{
				rayPictureImageView.SetImageBitmap(imageBitmap);
				//imageBitmap = null;
			}

			//required to avoid memory leaks!
			GC.Collect();
		}


		protected override void OnSaveInstanceState(Bundle outState)
		{
			base.OnSaveInstanceState(outState);
			outState.PutParcelable("bitmap", imageBitmap);
		}

	}
}