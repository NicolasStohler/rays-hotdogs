﻿using Android.App;
using Android.Widget;
using Android.OS;
using RaysHotDogs.Core.Service;

namespace RaysHotDogs
{
	[Activity(Label = "RaysHotDogs")]
	public class MainActivity : Activity
	{
		int count = 0;
		int codeIndex = 0;
		private HotDogDataService _HotDogDataService = new HotDogDataService();
		Button _button;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			_button = FindViewById<Button>(Resource.Id.MyButton);

			_button.Click += Button_Click;
			//button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };
			TextView codeTextView = FindViewById<TextView>(Resource.Id.codeTextView);

			codeTextView.Text = "";

		}

		private async void Button_Click(object sender, System.EventArgs e)
		{
			_button.Text = string.Format("{0} clicks!...", count);

			//var codes = _HotDogDataService.GetAllCodes();
			//var code = (codeIndex < codes.Count) ? codes[codeIndex++] : null;

			//_button.Text = string.Format("{0} clicks! {1}", count++, code?.Value ?? "none");
			TextView codeTextView = FindViewById<TextView>(Resource.Id.codeTextView);
			codeTextView.Text = "...";

			var codes = await _HotDogDataService.GetCodeListAsync();
			//var code = (codeIndex < codes.Count) ? codes[codeIndex] : null;
			var code = codes[codeIndex % codes.Count];

			codeTextView.Text = string.Format($"{code?.Date.ToShortDateString()}: {code?.Value}");

			_button.Text = string.Format("{0} clicks!", count);
			count++;
			codeIndex++;
		}
	}
}

