using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using RaysHotDogs.Core.Service;
using Android.Support.V7.App;
using Android.Support.Design.Widget;

namespace RaysHotDogs
{
	[Activity(Label = "Ray's Hot Dogs", MainLauncher = true, Theme = "@style/Theme.AppCompat.Light.DarkActionBar")]
	public class MenuActivity : AppCompatActivity
	{
		private Button orderButton;
		private Button cartButton;
		private Button aboutButton;
		private Button mapButton;
		private Button takePictureButton;
		private TextView dataServiceStateText;
		private Button testButton;
		private EditText editTextFirstName;
		private EditText editTextLastName;


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.MainMenu);

			FindViews();

			HandleEvents();

			// setting activity icon:
			// http://stackoverflow.com/questions/26440279/show-icon-in-actionbar-toolbar-with-appcompat-v7-21/26890240#26890240

			//this.ActionBar.SetHomeButtonEnabled(true);
			//this.ActionBar.SetDisplayShowHomeEnabled(true);
			//this.ActionBar.SetDisplayUseLogoEnabled(true);
			//this.ActionBar.SetLogo(Resource.Drawable.smallicon);
			//this.ActionBar.SetDisplayShowTitleEnabled(true);
			
			this.SupportActionBar.SetHomeButtonEnabled(true);
			this.SupportActionBar.SetDisplayShowHomeEnabled(true);
			this.SupportActionBar.SetIcon(Resource.Drawable.smallicon);
			this.SupportActionBar.SetDisplayShowTitleEnabled(true);

			
			ThreadPool.QueueUserWorkItem(o => InitiateDataService());
		}

		private void InitiateDataService()
		{
			RunOnUiThread(() => dataServiceStateText.Text = "loading...");
			HotDogDataService dataService = new HotDogDataService();
			RunOnUiThread(() => dataServiceStateText.Text = "ready!");
		}

		private void FindViews()
		{
			orderButton            = FindViewById<Button>(Resource.Id.orderButton);
			cartButton             = FindViewById<Button>(Resource.Id.cartButton);
			aboutButton            = FindViewById<Button>(Resource.Id.aboutButton);
			mapButton              = FindViewById<Button>(Resource.Id.mapButton);
			takePictureButton      = FindViewById<Button>(Resource.Id.takePictureButton);
			dataServiceStateText   = FindViewById<TextView>(Resource.Id.dataServiceState);
			//passEdit             = FindViewById<EditText>(Resource.Id.txtPass);
			testButton             = FindViewById<Button>(Resource.Id.testButton);
			editTextFirstName      = FindViewById<EditText>(Resource.Id.editTextFirstName);
			editTextLastName       = FindViewById<EditText>(Resource.Id.editTextLastName);
		}

		private void HandleEvents()
		{
			orderButton.Click += OrderButton_Click;
			aboutButton.Click += AboutButton_Click;
			takePictureButton.Click += TakePictureButton_Click;
			mapButton.Click += MapButton_Click;			

			testButton.Click += (sender, e) => {
				var view = FindViewById(Android.Resource.Id.Content);
				if (string.IsNullOrWhiteSpace(editTextFirstName.Text))
				{					
					Snackbar.Make(view, "Fill in username.", Snackbar.LengthLong)
						.SetAction("OK", (v) => { })
						.Show();
				}
				else if (editTextLastName.Text != "monkey")
				{
					Snackbar.Make(view, "Invalid Password", Snackbar.LengthLong)
							.SetAction("Clear", (v) => { editTextLastName.Text = string.Empty; })
							.Show();
				}
				else
				{
					Snackbar.Make(view, "You are now Logged in!", Snackbar.LengthLong)
							.Show();
				}
			};
		}

		private void MapButton_Click(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(RayMapActivity));
			StartActivity(intent);
		}

		private void TakePictureButton_Click(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(TakePictureActivity));
			StartActivity(intent);
		}

		private void AboutButton_Click(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(AboutActivity));
			StartActivity(intent);
		}

		private void OrderButton_Click(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(HotDogMenuActivity));
			StartActivity(intent);
		}
	}
}