﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceTest
{
	class Program
	{
		static void Main(string[] args)
		{
			var imageUrl = @"https://crackberry.com/sites/crackberry.com/files/styles/large/public/topic_images/2013/ANDROID.png";

			var runner = new Runner();

			Console.WriteLine("starting...");
			Task.Run(() => {
				var bytes = runner.RunAsync(imageUrl).Result;

				Console.WriteLine("saving image");
				using (Image image = Image.FromStream(new MemoryStream(bytes)))
				{
					image.Save("output.jpg", ImageFormat.Jpeg);  // Or Png
				}

			}).Wait();
			Console.WriteLine("done.");
		}

		
	}

	public class Runner
	{
		public async Task<byte[]> RunAsync(string url)
		{
			Console.WriteLine("new http client");
			using (var httpClient = new HttpClient())
			{
				try
				{
					Task<HttpResponseMessage> getResponse = httpClient.GetAsync(url);
					Console.WriteLine("response");

					HttpResponseMessage response = await getResponse;
					Console.WriteLine("response got");

					var bytes = await response.Content.ReadAsByteArrayAsync();
					Console.WriteLine("bytes read");
					//var responseJsonString = await response.Content.ReadAsStringAsync();
					return bytes;
				}
				catch (Exception ex)
				{
					Console.WriteLine("{0}: {1}", ex.GetType().Name, ex.Message);
					//throw;
				}				
				return new byte[0];
			}
		}
	}
}
