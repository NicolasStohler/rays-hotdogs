﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RaysHotDogs.Core.Model;

namespace RaysHotDogs.Core.Repository
{
	public class ImageRepository
	{
		//private static Dictionary<int, byte[]> _hotDogIdImageMap = new Dictionary<int, byte[]>();
		// var imageBitmap = ImageHelper.GetImageBitmapFromUrl("http://gillcleerenpluralsight.blob.core.windows.net/files/" + item.ImagePath + ".jpg");


		//public static byte[] GetImageBitmapFromUrl(string url)
		//{
		//	//Bitmap imageBitmap = null;
		//	using (var httpClient = new HttpClient())
		//	{
		//		Task<HttpResponseMessage> getResponse = httpClient.GetAsync(url);

		//		HttpResponseMessage response = await getResponse;

		//		responseJsonString = await response.Content.ReadAsStringAsync();
		//	}

		//	using (var webClient = new WebClient())
		//	{
		//		var imageBytes = webClient.DownloadData(url);
		//		return imageBytes;
		//		//if (imageBytes != null && imageBytes.Length > 0)
		//		//{
		//		//	imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
		//		//}
		//	}

		//	return null;
		//}
		internal static void LoadImagesAsync(List<HotDog> hotDogs)
		{
			List<Task> taskList = new List<Task>();
			foreach (var hotDog in hotDogs)
			{
				var task = Task.Run(() => getImageAsync(hotDog));
				taskList.Add(task);
			}
			Task.WaitAll(taskList.ToArray());
		}

		private static async Task getImageAsync(HotDog hotDog)
		{
			var url = "http://gillcleerenpluralsight.blob.core.windows.net/files/" + hotDog.ImagePath + ".jpg";
			using (var httpClient = new HttpClient())
			{
				try
				{
					Task<HttpResponseMessage> getResponse = httpClient.GetAsync(url);

					HttpResponseMessage response = await getResponse;

					var bytes = await response.Content.ReadAsByteArrayAsync();

					hotDog.ImageBytes = bytes;
					//var responseJsonString = await response.Content.ReadAsStringAsync();
					
				}
				catch (Exception ex)
				{
					//Console.WriteLine("{0}: {1}", ex.GetType().Name, ex.Message);
					throw;
				}
				//return new byte[0];
			}
		}
	}
}
