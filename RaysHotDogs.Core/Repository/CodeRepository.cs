﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Firebase.Database.Query;

namespace RaysHotDogs.Core.Repository
{
	public class CodeRepository
	{
		// get codes from firebase rest api
		public List<Code> GetAllCodes()
		{
			var codes = GetAllCodesAsync();
			var result = codes.Select(x => x.Object);
			return result.ToList();
		}

		public async Task<List<Code>> GetCodeListAsync()
		{
			var firebase = new FirebaseClient("https://ngfirebasechataaa.firebaseio.com/");

			var codeSet = await firebase
				.Child("code")
				.OrderByKey()
				.OnceAsync<Code>();

			return codeSet.Select(x => x.Object).ToList();
		}

		//public async Task<>

		private List<FirebaseObject<Code>> GetAllCodesAsync()
		{
			var firebase = new FirebaseClient("https://ngfirebasechataaa.firebaseio.com/");

			var codeSet = firebase
				.Child("code")
				.OrderByKey()
				.OnceAsync<Code>();

			codeSet.Wait();

			return codeSet.Result.ToList();
		}
	}

	public class Code
	{
		public string Value { get; set; }
		public DateTime Date { get; set; }
	}
}
